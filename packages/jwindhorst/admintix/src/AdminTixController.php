<?php

namespace jwindhorst\AdminTix;

use App\Http\Controllers\Controller;
use Carbon\Carbon;

class AdminTixController extends Controller {

    public function index()
    {
        return view('AdminTix::index');
    }
}
