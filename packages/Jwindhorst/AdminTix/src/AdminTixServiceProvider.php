<?php

namespace Jwindhorst\AdminTix;

use Illuminate\Support\ServiceProvider;

class AdminTixServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadViewsFrom(__DIR__.'/views', 'AdminTix');
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        include __DIR__.'/routes/web.php';
        $this->app->make('Jwindhorst\AdminTix\AdminTixController');
    }
}
