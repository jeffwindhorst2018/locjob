<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * This is a raw pull storaage table. The data inside this table should NEVER
 * be changed or referenced in the app directly.
 */
class CreateSimpleMapsRawData extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // CSV Data Map
        //
        //"city","city_ascii","state_id","state_name","county_fips","county_name","lat","lng","population",
        //"population_proper","density","source","incorporated","timezone","zips","id"
        //
        Schema::create('simple_maps_raw', function (Blueprint $table) {
            $table->increments('id');
            $table->string('city');
            $table->string('city_ascii');
            $table->string('state_id');
            $table->string('state_name');
            $table->string('county_fips');
            $table->string('county_name');
            $table->decimal('lat', 10, 8);
            $table->decimal('lng', 10, 8);
            $table->integer('population')->nullable()->unsigned();
            $table->integer('population_proper')->nullable()->unsigned();
            $table->decimal('density', 10, 8);
            $table->string('source');
            $table->integer('incorporated', false, true);
            $table->string('timezone');
            $table->text('zips');
            $table->string('uid');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
