<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLifestyleChoices extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('lifestyle_choices', function (Blueprint $table) {
            Schema::create('lifestyle_choices', function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->string('choice_table');
                $table->timestamps();
            });
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('lifestyle_choices', function (Blueprint $table) {
            //
        });
    }
}
