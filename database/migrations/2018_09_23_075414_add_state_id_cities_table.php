<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddStateIdCitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::disableForeignKeyConstraints();
        if (Schema::hasColumn('cities', 'state_id'))
        {
            Schema::table('cities', function (Blueprint $table)
            {
//                $table->dropForeign(['state_id']);
                $table->dropColumn('state_id');
            });
        }
        Schema::table('cities', function (Blueprint $table) {
            $table->integer('state_id')->unsigned()->nullable()->after('id');
            $table->foreign('state_id')->references('id')->on('states');
        });
        
        Schema::table('cities', function (Blueprint $table) {
            $sql = "UPDATE `cities`
                LEFT JOIN `states` ON `cities`.`state` = `states`.`name`
                SET `state_id` = `states`.`id`
                ";
            echo $sql;
            DB::statement($sql);
        });
        Schema::enableForeignKeyConstraints();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('cities', function (Blueprint $table) {
            $table->dropForeign(['state_id']);
            $table->dropColumn('state_id');
        });
    }
}
