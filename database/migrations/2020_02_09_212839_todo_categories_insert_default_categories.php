<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class TodoCategoriesInsertDefaultCategories extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('todo_categories', function (Blueprint $table) {
            DB::table('todo_categories')->insert([
                ['category' => 'Bug', 'description' => 'Defect in application'],
                ['category' => 'User Story', 'description' => 'Work that has not yet been done.'],
                ['category' => 'Todo', 'description' => 'Clean up or nice to haves'],
            ]);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('todo_categories', function (Blueprint $table) {
            //
        });
    }
}
