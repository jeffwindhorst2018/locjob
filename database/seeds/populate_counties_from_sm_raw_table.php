<?php

use Illuminate\Database\Seeder;

class populate_ccounties_from_sm_raw_table extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        /*
         * WORKING QUERY
         * UPDATE cities
         * -- select cities.county_id, counties.id
         * -- FROM cities
         * LEFT JOIN states ON cities.state_id = states.id
         * LEFT JOIN simple_maps_raw ON cities.name = simple_maps_raw.city
         * LEFT JOIN counties ON counties.county_name = simple_maps_raw.county_name
         * SET cities.county_id = counties.id
         * WHERE states.abbreviation = simple_maps_raw.state_id
         * AND cities.name = simple_maps_raw.city
         * AND cities.state_id = states.id
         * AND states.abbreviation = simple_maps_raw.state_id
         */
        DB::table('counties')->truncate();
        
        $sql = "INSERT INTO `counties` (`counties`.`county_name`)
                SELECT DISTINCT simple_maps_raw.county_name
                FROM simple_maps_raw";
        DB::statement($sql);
    }
}
