<?php

use Illuminate\Database\Seeder;
use App\Source;
use Flynsarmy\CsvSeeder\CsvSeeder;

class SourcesSimpleMapsSeeder extends CsvSeeder
{
    public function  __construct() {
        $this->table = 'simple_maps_raw';
        $this->filename = base_path() . '/database/seeds/csvs/uscitiesv1.4.csv';
        $this->insert_chunk_size = 500;
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::disableQueryLog();
        DB::table($this->table)->truncate();

        parent::run();
    }
}
