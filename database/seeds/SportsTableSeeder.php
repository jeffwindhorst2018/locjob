<?php

use Illuminate\Database\Seeder;
use App\Sports;

class SportsTableSeeder extends Seeder
{
    private $sports = [
        [
            'acronym' => 'mlb',
            'fullname' => 'Major League Baseball',
            'source' => 'https://en.wikipedia.org/wiki/Major_League_Baseball',
            'datafile' => 'mlb_data.html'
        ],
        [
            'acronym' => 'nba',
            'fullname' => 'National Basketball Association',
            'source' => 'https://en.wikipedia.org/wiki/National_Basketball_Association',
            'datafile' => 'nba_data.html'
        ],
        [
            'acronym' => 'nfl',
            'fullname' => 'National Football League',
            'source' => 'https://en.wikipedia.org/wiki/National_Football_League',
            'datafile' => 'nfl_data.html'
        ],
        [
            'acronym' => 'nhl',
            'fulname' => 'National Hockey League',
            'source' => 'https://en.wikipedia.org/wiki/National_Hockey_League',
            'datafile' => 'nhl_data.html'
        ],
        [
            'acronym' => 'mls',
            'fullname' => 'Major League Soccer',
            'source' => 'https://en.wikipedia.org/wiki/Major_League_Soccer',
            'datafile' => 'mls_data.html'
        ]
    ];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('sports')->insert($this->sports);
    }
}
