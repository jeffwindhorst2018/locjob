<?php

use Illuminate\Database\Seeder;
use \App\City;
use \Carbon\Carbon;

class TransitLinesSeeder extends Seeder
{

    protected $transitUrl = 'https://en.wikipedia.org/wiki/List_of_rail_transit_systems_in_the_United_States';

    protected $ridershipUrl = 'https://en.wikipedia.org/wiki/List_of_U.S._cities_with_high_transit_ridership';

    protected $baseUrl = 'https://en.wikipedia.org/';
    
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $contextOptions = [
            'ssl' => [
                'verify_peer' => false,
                'verify_peer_name' => false,
            ]
        ];

        $dataFiles = [
                        'transit' => [
                                        'file' => 'data/transit_data.html',
                                        'url' => $this->transitUrl,
                                        'map' => [
                                            'region',
                                            'state',
                                            'system',
                                            'lines',
                                            'authority',
                                            'type'
                                        ],
                                    ],
                        'ridership' => [
                                        'file' => 'data/transit_ridership_data.html',
                                        'url' => $this->ridershipUrl,
                                    ]
        ];

        foreach($dataFiles as $data)
        {
            if(!Storage::exists($data['file'])) {
                echo 'Getting transit data from: ' . $data['url'] . "\n";

                $dataFile = file_get_contents($data['url'], false, stream_context_create($contextOptions));
                Storage::put($data['file'], $dataFile);
            }
        }

        $ridershipArr = [];
        $ridershipXml = simplexml_load_string(Storage::get($dataFiles['ridership']['file']));
        
        $ridershipRows = $ridershipXml->xpath('//div[contains(@class, "mw-parser-output")]/p');
        $ri = 0;
        foreach($ridershipRows as $rrow) {
            if($ri >= 1) {
                list($city, $state) = explode(',', $rrow->a);
                $ridership = str_replace('%', '', explode(' ', $rrow)[3]);
                $ridershipArr[$city] = $ridership;
            }
            $ri++;
        }

        $transitXml = simplexml_load_string(Storage::get($dataFiles['transit']['file']));
        $rows = $transitXml->xpath('//table[contains(@class, "wikitable")][1]/tbody/tr');
        echo 'Proessing ' . count($rows) . " transit rows. \n";
        $i = 0;

        foreach($rows as $row) {

            $rs = [];
            if($i >= 1) {
                $city = $row->td[0]->a;
                $cityId = $this->getCityId($city);
                $system = $row->td[2]->a;
                $systemLink = $this->baseUrl . $row->td[2]->a['href'];

                if(isset($ridershipArr[strtolower($city)])) {
                    $rs[] = $city;
                }
         
//                \DB::table('tranit_lines')->insert([
//                        'city_id' => $cityId,
//                        'system' => $system,
//                        'ridership' => $ridershipArr,
//                        'created_at' => Carbon::now()
//                ]);
//                exit;

            }
            $i++;

        }
    }

    /**
     *
     * @param String $city
     * @return Integer
     */
    private function getCityId($city)
    {
        $city = ('Jersey City' == $city) ? 'New Jersey' : $city;
        $city = str_replace('Greater ', '', $city);

        $cityRes = City::where('name', 'LIKE', "%{$city}%")->first();
        if(!$cityRes)
        {
            echo 'FAILED ' . $city . "\n";
            print_r(City::where('name', 'LIKE', "%{$city}%")->get());
        }
        
//        return $cityRes->id;
    }
}
