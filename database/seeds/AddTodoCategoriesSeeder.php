<?php

use Illuminate\Database\Seeder;
use \App\TodoCategory;

class TodoCategoriesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('todo_categories')->truncate();

        TodoCategory::create([
                'category' => 'Tasks',
                'description' => 'Already defined, ready to be assigned and worked on.',
            ]);
        TodoCategory::create([
                'category' => 'Ideas',
                'description' => 'Far reaching ideas, not thoroughly fleshed out.',
        ]);
        TodoCategory::create([
                'category' => 'Reasearch',
                'description' => 'Idea has been agreed upon, now how do we acutally implement it.',
        ]);
    }
}
