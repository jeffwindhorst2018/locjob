<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class TransitTypeSeeder extends Seeder
{

    protected $transitTypes = [];

    public function __construct() {
        $this->transitTypes = [
            [
                'name' => 'Automated guideway transit',
                'link' => 'https://en.wikipedia.org/wiki/Automated_guideway_transit',
                'created_at' => Carbon::now()
            ],
            [
                'name' => 'Commuter rail',
                'link' => 'https://en.wikipedia.org/wiki/Commuter_rail',
                'created_at' => Carbon::now()
            ],
            [
                'name' => 'Automated light rail',
                'link' => 'https://en.wikipedia.org/wiki/Automatic_train_operation',
                'created_at' => Carbon::now()
            ],
            [
                'name' => 'People mover',
                'link' => 'https://en.wikipedia.org/wiki/People_mover',
                'created_at' => Carbon::now()
            ],
            [
                'name' => 'Commuter rail',
                'link' => 'https://en.wikipedia.org/wiki/Commuter_rail',
                'created_at' => Carbon::now()
            ],
            [
                'name' => 'Heavy rail',
                'link' => 'https://en.wikipedia.org/wiki/Passenger_rail_terminology#Heavy_rail',
                'created_at' => Carbon::now()
            ],
            [
                'name' => 'Heritage cable car',
                'link' => 'https://en.wikipedia.org/wiki/Cable_car_(railway)',
                'created_at' => Carbon::now()
            ],
            [
                'name' => 'Light rail',
                'link' => 'https://en.wikipedia.org/wiki/Light_rail',
                'created_at' => Carbon::now()
            ],
            [
                'name' => 'Monorail',
                'link' => 'https://en.wikipedia.org/wiki/Monorail',
                'created_at' => Carbon::now()
            ],
            [
                'name' => 'Streetcar',
                'link' => 'https://en.wikipedia.org/wiki/Tram',
                'created_at' => Carbon::now()
            ]
        ];
    }
    
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('transit_types')->insert($this->transitTypes);
    }
}
