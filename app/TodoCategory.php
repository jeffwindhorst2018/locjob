<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property string $category
 * @property string $description
 * @property string $created_at
 * @property string $updated_at
 */
class TodoCategory extends Model
{
    /**
     * @var array
     */
    protected $fillable = ['category', 'description', 'created_at', 'updated_at'];

    public function todo()
    {
        return $this->belongsTo('App\Todo');
    }
}
