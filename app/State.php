<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property string $name
 * @property string $abbreviation
 * @property string $created_at
 * @property string $updated_at
 */
class State extends Model
{
    public $timestamps = true;
    
    /**
     * @var array
     */
    protected $fillable = ['name', 'abbreviation', 'created_at', 'updated_at'];

    public function city() {
        return $this->hasMany('App\City');
    }

}
