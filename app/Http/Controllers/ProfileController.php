<?php

namespace App\Http\Controllers;

use App\User;
use Auth;

use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

use Session;
use Illuminate\Http\Request;

use App\LifestyleChoice;
use App\Sport;

class ProfileController extends Controller
{
    public function __construct() {
        $this->middleware('auth');
    }

    /**
     * Display a users primary profile page.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $lifestyleChoices = LifestyleChoice::all();
        $sports = Sport::all();

        // $profileData = Profile::all();
        return view('profile.index', compact('lifestyleChoices', 'sports'));

        // return view('profile.index');
    }
}
