<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\User;
use Auth;

use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

use Session;

class UserController extends Controller
{
    public function __construct() {
        $this->middleware('auth');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::all();
        return view('admin.users.index')->with('users', $users);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $roles = Role::get();
        return view('admin.users.create', ['roles'=>$roles]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|max:120',
            'email' => 'required|email|unique:users',
            'password' => 'required|min:6|confirmed'
        ]);
        
        $user = User::create($request->only('email', 'name', 'password'));
    
        $roles = $request['roles'];
        
        if(isset($roles)) {
            foreach($roles as $role) {
                $role_r = Role::where('id', '=', $role)->firstOrFail();
                $user->assignRole($role_r);
            }
        }
        
        return redirect()->route('users.index')
                ->with('flash_meesage',
                    'User successfully added.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return redirect('admin.user');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::findOrFail($id);
        $roles = Role::get();
        
        return view('admin.users.edit', compact('user', 'roles'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user = User::findOrFail($id);

        $inputs = $request->only(['name', 'email']);
        $editVals = [
            'name' => 'required|max:120',
            'email' => 'required|email|unique:users,email,'.$id,
        ];
        if(!empty($request['password'])) {
            $editVals['password'] = 'required|min:6|confirmed';
            $inputs['password'];
        }

    $this->validate($request, $editVals);
    $input = $request->only($inputs);
    $user->fill($input)->save();

    if(!empty($request['roles'])) {
        $user->roles()->sync($request['roles']);
    
    } else {
        $user->roles()->detach();
    }
    return redirect()->route('users.index')
            ->with('flash_message',
            'User succesfully edited.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\User
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        // $user = User::findOrFail(User $id);
        $user->delete();
        
        return redirect()->route('users.index')
                ->with('flash_message',
                    'User successfully deleted.');
    }
}
