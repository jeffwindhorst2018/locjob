<?php

namespace App\Http\Controllers;

use App\Todo;
use Illuminate\Http\Request;
use DataTables;

class TodoAjaxController extends Controller
{

        public function __construct()
    {
        // TODO Fix this so that only todo editors have access.
         $this->middleware(['auth', 'clearance']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {
            // TODO I think we will need to add categories and users to the with clause
            //
            // Found this snippet here  https://medium.com/@BishoAtif/integrating-laravel-with-datatables-975b0bbf4009
            $dtRet = Datatables::of(Todo::query()->with(['TodoStatus', 'TodoCategory', 'User']))
//                    ->addIndexColumn()
                    ->removeColumn('status.id')
                    ->addColumn('todo_status', function($row) {
                        return $row->TodoStatus->status;
                    })
                    ->addColumn('todo_category', function($row) {
                        return $row->TodoCategory->category;
                    })
                    ->addColumn('user', function($row) {
                        return $row->User->name;
                    })
                    ->addColumn('action', function($row){
                           $btn = '<a href="javascript:void(0)" data-toggle="tooltip"  data-id="'.$row->id.'" data-original-title="Edit" class="edit btn btn-primary btn-sm editTodo">Edit</a>';
                           $btn = $btn.' <a href="javascript:void(0)" data-toggle="tooltip"  data-id="'.$row->id.'" data-original-title="Delete" class="btn btn-danger btn-sm deleteTodo">Delete</a>';

                            return $btn;
                    })
                    ->rawColumns(['action'])
                    ->make(true);

            return $dtRet;
        }
        
        return view('/admin/todo/todoAjax',compact('todos'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Todo::updateOrCreate(['id' => $request->todo_id],
                ['title' => $request->title, 'description' => $request->description]);

        return response()->json(['success'=>'Todo saved successfully.']);
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Todo  $todo
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $todo = Todo::find($id);
        return response()->json($todo);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Todo  $todo
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Todo::find($id)->delete();

        return response()->json(['success'=>'Todo deleted successfully.']);
    }
}
