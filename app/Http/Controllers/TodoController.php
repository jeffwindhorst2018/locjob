<?php

namespace App\Http\Controllers;

use App\Todo;
use Illuminate\Http\Request;
use App\User;
use App\TodoStatus;
use App\TodoCategory;
use Illuminate\Support\Arr;
use Validator;
use Response;

class TodoController extends Controller
{

    public function __construct()
    {
        // TODO Fix this so that only todo editors have access.
         $this->middleware(['auth', 'clearance'])->except(['index', 'store', 'show', 'edit', 'destroy', 'update', 'updateAjax']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $todos = Todo::with(['todoStatus', 'user'])->orderby('created_at')->paginate(25);
        
        $todoStatuses = TodoStatus::select('id', 'status')->get();
        $todoCategories = TodoCategory::select('id', 'category')->get();
        
        $returnVars = compact('todos');
        $returnVars['todoStatuses'] = $todoStatuses;
        $returnVars['todoCategories'] = $todoCategories;

        $totalTodos = $returnVars['totalTodos'] = Todo::count();
        $todoUsers = $returnVars['todoUsers'] = \Auth::User()->role('Super Admin')->pluck('name', 'id');
        return view('admin.todo.index', $returnVars);
        
    }

    /**
     *
     * @param collection $data
     * @return HTML todos table 
     */
    private function modalTable($data) {

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        // Validate form
        $validator = Validator::make($request->all(), [
            'title'=>'required|max:100',
            'description' =>'required',
            'status' =>'required',
            'category' =>'required',
        ]);
        
        if($validator->passes()) {
            $todo = Todo::create([
                'title' => $request['title'],
                'description' => $request['description'],
                'category_id' => $request['category'],
                'status_id' => $request['status'],
                'user_id' => $request['user']
            ]);

//            $todoCount = Todo::where('status_id', $request['status'])->count();
//            $statusT = Todo::where('status_id', $request['status'])->pluck('title');

//            return response()->json([
//                        'success'=>'Added new todo.',
//                        'status' => $request['status'],
//                        'todoCount' => $todoCount,
//                        'statusName' => $satusTitle,
//                        'mode' => $request['mode']
//
//                    ]);
            return Response::json($todo);
        }
        return response()->json(['error' => $validator->errors()->all()]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Todo  $todo
     * @return \Illuminate\Http\Response
     */
    public function show($todoId)
    {
        $todo = Todo::where('id', $todoId)->first();
        
        return view('admin.todo.show', ['todo' => $todo]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Todo  $todo
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request)
    {
        $todoId = last(explode('/', $request->path()));
        $todo = \App\Todo::find($todoId);
        $returnVars = $usersArr = $statusArr = $categoryArr = [];

        $returnVars = [
            'todo'  => $todo,
            'todoUsers' => \Auth::User()->role('Super Admin')->pluck('name', 'id'),
            'statuses'  => TodoStatus::pluck('status', 'id'),
            'categories'  => TodoCategory::pluck('category', 'id'),
        ];

        return view('admin.todo.edit', $returnVars);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Todo  $todo
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $todo = \App\Todo::find($request->input('id'));
        $todo->todo_category_id = ( null !== $request->input('category') ) ? $request->input('category') : $todo->todo_category_id;
        $todo->title = ( null !== $request->input('title') ) ? $request->input('title') : $todo->title;
        $todo->description = ( null !== $request->input('description') ) ? $request->input('description') : $todo->description;
        $todo->user_id = ( null !== $request->input('user_id') ) ? $request->input('user_id') : $todo->user_id;
        $todo->status_id = ( null !== $request->input('status_id') ) ? $request->input('status_id') : $todo->status_id;
        $todo->save();

        if(! $request->ajax())
        {
            return redirect()->route('todos.index')
                ->with('flash_message', 'Todo, ' . $todo->title . ' updated.');
        } else {
            echo json_encode(['res' => 1]);
        }
    }

    public function updateAjax(Request $request)
    {
        $todo = \App\Todo::find($request->input('todoId'));
        $field = $request->input('field');
        $value = $request->input('val');

        $todo->$field = $request->input('val');
        echo json_encode(['res' => $todo->save()]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Todo  $todo
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $title = \App\Todo::where('id', $request->id)->pluck('title')->flatten();
        \App\Todo::destroy($request->id);
        echo json_encode(['message' => "Todo $title was deleted", 'title' => $title]);
    }
}
