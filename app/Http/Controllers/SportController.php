<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Sport;
use Auth;
use Session;

class SportController extends Controller
{
    
    public function __construct()
    {
         $this->middleware(['auth', 'clearance'])->except('index', 'show');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $sports = Sport::orderby('id', 'name')->paginate(25); //show only 5 items at a time in descending order
        $returnVars = compact('sports');
        $returnVars['totalSports'] = Sport::count();
        
        return view('admin.sport.index', $returnVars);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('sport.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'acronym'=>'required|max:3',
            'fullname' =>'required',
            'source' =>'required',
            'datafile' =>'required',
            ]);

        $acronym = $request['acronym'];
        $fullname = $request['fullname'];
        $source = $request['source'];
        $datafile = $request['datafile'];

        $sport = Sport::create($request->only('acronym', 'fullname', 'source', 'datafile'));

    //Display a successful message upon save
        return redirect()->route('sport.index')
            ->with('flash_message', 'Sport,
             '. $sport->acronym .' created');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        $city = Sport::with('SportsTeam')->where('id', $request->input('id'))->first();
        echo json_encode($sport);
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $sport = Sport::findOrFail($id);
        
        return view('sports.edit', compact('sport'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'acronym' => 'required',
            'fullname' => 'required',
            'source' => 'required',
            'datafile' => 'required',
        ]);
        
        $sport = Sport:: findOrFail($id);
        $sport->acronym = $request->input('acronym');
        $sport->fullname = $request->input('fullname');
        $sport->source = $request->input('source');
        $sport->datafile = $request->input('datafile');
        $sport->save();
        
        return redirect()->route('sports.show',
                $sport->id)->with('flash_message',
                'Sport, '. $sport->name. ' updated.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $sport = Sport::findOrFail($id);
        $sportAcronym = $sport->acronym;
        $sport->delete();
        
        return redirect()->route('sport.index')
                ->with('flash_message',
                        $sportAcronym . ' successfully deleted.');
    }
}
