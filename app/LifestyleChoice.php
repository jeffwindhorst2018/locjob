<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LifestyleChoice extends Model
{
    protected $table = 'lifestyle_choices';

    /**
     * @var array
     */
    protected $fillable = ['choice_table', 'created_at', 'updated_at'];
}
