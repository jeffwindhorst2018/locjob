<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property string $county_name
 * @property string $created_at
 * @property string $updated_at
 */
class County extends Model
{
    /**
     * @var array
     */
    protected $fillable = ['county_name', 'created_at', 'updated_at'];

    protected $table = 'counties';

}
