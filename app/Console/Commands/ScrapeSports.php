<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Goutte\Client;
use Illuminate\Support\Facades\Storage;
use \App\Sport;
use \App\City;
use \App\SportsTeam;

class ScrapeSports extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'scrape:sports {sport?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Scrape sports teams from Wikipedia';

    private $sports;
    
    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();

        // get all the sports information from the DB
        $this->sports = Sport::all();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        if(Sport::count() == 0) {
            $this->error('You must seed the sports table before running this script.');
        }

        $sport = $this->argument('sport');
        $filename = 'data/';

        if(isset($sport)) {
            $esport = $this->sports->firstWhere('acronym', $sport);
            $sportId = $esport->id;
            
            if(NULL === $esport) {
                $this->error('Invalid sport.');
            } else {
                $filename = 'data/' . $esport->datafile ;
                $this->storeDataFile($filename, $esport->source);
            }
        } else {
            foreach($this->sports as $key => $asport) {
                $this->storeDataFile('data/' . $sport['datafile'], $sport['source']);
            }
        }

        $xml = simplexml_load_string(Storage::get($filename));

        
        // XPATH works for MLB
        $rows = $xml->xpath('//table[contains(@class, "wikitable")][1]/tbody/tr'); // Sports teams

        switch($sport) {
            case 'mlb':
                $this->scrapeMLB($rows, $sportId);
                break;
            case 'nba':
                $this->scrapeNBA($rows, $sportId);
                break;
            case 'nfl':
                $this->scrapeNFL($rows, $sportId);
                break;
            case 'nhl':
                $this->scrapeNHL($rows, $sportId);
                break;
            case 'mls':
                $this->scrapeMLS($rows, $sportId);
                break;
            default:
                echo 'Loop through all.';
                exit;
        }

        
    }

    private function scrapeMLS($rows, $sportId) {
        echo 'MLS ' . count($rows) . ' ' . $sportId . "\n";
        $i = 0;

        foreach($rows as $row) {
            if($i >= 2) {
                if($i == 27) break;
                if($i !== 13 && $i !== 26) {
                    $team = $row->td[0]->b->a;
                    $city = trim(explode(',', $row->td[1]->a[0])[0]);
                    $homepark = $row->td[2]->a;
                    
                    if(
                        $city !== 'Bridgeview' &&
                        $city !== 'Montreal' &&
                        $city !== 'Foxborough' &&
                        $city !== 'Harrison' &&
                        $city !== 'Chester' &&
                        $city !== 'Toronto' &&
                        $city !== 'Vancouver' &&
                        trim($city) !== 'Landon'
                    ) {

                        $ecity = City::where('name', '=', trim($city))->first(['id']);
                        $cityId = $ecity->id;
                        $capacity = $row->td[2]->a;

                        \DB::table('sports_teams')->insert([
                                'sport_id' => $sportId,
                                'city_id' => $cityId,
                                'team' => $team,
                                'homepark' => $homepark,
                                'capacity' => $capacity,
                                'created_at' => time()
                        ]);
                    }
                }
            }
            $i++;
        }
    }

    private function scrapeNHL($rows, $sportId) {
        echo 'NHL ' . count($rows) . ' ' . $sportId . "\n";
        $i = 0;
        foreach($rows as $row) {
            if($i >= 2) {
                if($i !== 18) {
                    $team = $row->td[0]->b->a;

                    $city = trim(explode(',', $row->td[1]->a[0])[0]);
                    $city = ( $city == 'Saint Paul' ) ? 'St. Paul' : $city;
                    $city = ( $city == 'Nashville' ) ? 'Nashville-Davidson' : $city;
                    $city = ( $city == 'Paradise' ) ? 'Las Vegas' : $city;

                    $homepark = $row->td[2]->a;
                    echo $city . ' ' . $homepark . ' ' . $i . "\n";

                    if(
                            $city !== 'Montreal' &&
                            $city != 'Ottawa' &&
                            $city != 'Toronto' &&
                            $city != 'Winnipeg' &&
                            $city != 'Calgary' &&
                            $city != 'Edmonton'
                        ) {
                        $ecity = City::where('name', '=', trim($city))->first(['id']);
                        $cityId = $ecity->id;

                        $capacity = $row->td[2]->a;

                        \DB::table('sports_teams')->insert([
                                'sport_id' => $sportId,
                                'city_id' => $cityId,
                                'team' => $team,
                                'homepark' => $homepark,
                                'capacity' => $capacity,
                                'created_at' => time()
                        ]);
                    }
                }
            }
            $i++;
        }
    }

    private function scrapeNFL($rows, $sportId) {
        echo count($rows) . ' ' . $sportId . "\n";
        $i = 0;
        foreach($rows as $row) {
            if($i >= 2) {
                if($i !== 18) {
                    $team = $row->td[0]->b->a;

                    $city = trim(explode(',', $row->td[1]->a[0])[0]);
                    $homepark = $row->td[2]->a;
                    echo $city . ' ' . $homepark . ' ' . $i . "\n";

                    $city = ($city == 'Nashville') ? 'Nashville-Davidson' : $city;
                    // TODO add these cities to the database
                    if(
                            trim($city) !== 'Orchard Park' &&
                            trim($city) !== 'Foxborough' &&
                            trim($city) !== 'East Rutherford' &&
                            trim($city) !== 'Landover' &&
                            trim($city) !== 'Seattle')
                    {
                        // TODO there is a bug being thrown after the 24th record
                        $ecity = City::where('name', '=', trim($city))->first(['id']);
                        $cityId = $ecity->id;


                        $capacity = $row->td[2]->a;

                        \DB::table('sports_teams')->insert([
                                'sport_id' => $sportId,
                                'city_id' => $cityId,
                                'team' => $team,
                                'homepark' => $homepark,
                                'capacity' => $capacity,
                                'created_at' => time()
                        ]);
                    }
                }
            }
            $i++;
        }
    }

    private function scrapeNBA($rows, $sportId) {

        $i = 0;
        foreach($rows as $row) {
            if($i >= 2) {

                if($i !== 17) {
                    $team = $row->td[0]->b->a;
                    $city = $row->td[1]->a[0];
                    $homepark = $row->td[2]->a;
                    $capacity = $row->td[3];

                    $city = ($city == 'Washington, D.C.') ? 'Washington' : $city;
                    echo $city . ' ' . $i . "\n";
                    if( trim($city) !== 'Toronto') {

                        
                        $ecity = City::where('name', '=', $city)->first(['id']);
                        $cityId = $ecity->id;

                        \DB::table('sports_teams')->insert([
                            'sport_id' => $sportId,
                            'city_id' => $cityId,
                            'team' => $team,
                            'homepark' => $homepark,
                            'capacity' => $capacity,
                            'created_at' => time()
                        ]);
                    }
                }
            }
            $i++;
        }
    }

    private function scrapeMLB($rows, $sportId) {
        $i = 0;
        foreach($rows as $row) {

            if($i >= 2) {
                if($i !== 17) {
                    list($city, $state) = explode(',', $row->td[1]->a);
                    if($city !== 'Toronto')  {

                        $ecity = City::where('name', '=', $city)->first(['id']);
                        if(!$ecity) $this->error('City ' . $city . ' not found.');
                        $cityId = $ecity->id;
                        $team = $row->td->b->a;
                        $homepark = $row->td[2]->a;
                        $capacity = $row->td[3];

                        \DB::table('sports_teams')->insert([
                            'sport_id' => $sportId,
                            'city_id' => $cityId,
                            'team' => $team,
                            'homepark' => $homepark,
                            'capacity' => $capacity,
                            'created_at' => time()
                        ]);
                    }
                }
            }
            $i++;
        }
    }


    /**
     *
     * @param array $sport
     */
    public function storeDataFile($filename, $source) {
        $contextOptions = [
            'ssl' => [
                'verify_peer' => false,
                'verify_peer_name' => false,
            ]
        ];

        if(!Storage::exists($filename)) {
            $file = file_get_contents($source, false, stream_context_create($contextOptions));
            Storage::put($filename, $file);
        }
    }
    
}


