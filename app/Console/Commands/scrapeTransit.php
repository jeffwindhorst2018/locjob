<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Storage;

class ScrapeTransit extends Command {
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'scrape:transit';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Pull in a data list about transit lines from wikipedia, and populate the transit_lines table.';

    protected $dataUrl = 'https://en.wikipedia.org/wiki/List_of_rail_transit_systems_in_the_United_States';

    protected $ridershipUrl = 'https://en.wikipedia.org/wiki/List_of_U.S._cities_with_high_transit_ridership';


    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct() {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle() {
        $filename = 'data/transit_data.html';

        if(!Storage::exists($filename)) {
            echo 'Getting from: ' . $this->dataUrl . "\n";
            $contextOptions = [
                'ssl' => [
                    'verify_peer' => false,
                    'verify_peer_name' => false,
                ]
            ];

            $dataFile = file_get_contents($this->dataUrl, false, stream_context_create($contextOptions));
            Storage::put($filename, $dataFile);
        }

        $xml = simplexml_load_string(Storage::get($filename));

        $rows = $xml->xpath('//table[contains(@class, "wikitable")][1]/tbody/tr');
        echo 'Proessing ' . count($rows) . " rows. \n";
        $i = 0;

        foreach($rows as $row) {
            echo $i . "\n";

            
            if($i >= 1) {

                echo '<pre>';
                print_r($row);
                echo '</pre>';
                exit;
//                if($i == 27) break;
//                if($i !== 13 && $i !== 26) {


//                    if($i == 2) exit;
//                    $transitLine = $row->td[0]->b->a;
//                    $city = trim(explode(',', $row->td[1]->a[0])[0]);
//                    $homepark = $row->td[2]->a;
//
//                    if(
//                    $city !== 'Bridgeview' &&
//                            $city !== 'Montreal' &&
//                            $city !== 'Foxborough' &&
//                            $city !== 'Harrison' &&
//                            $city !== 'Chester' &&
//                            $city !== 'Toronto' &&
//                            $city !== 'Vancouver' &&
//                            trim($city) !== 'Landon'
//                    ) {
//
//                        $ecity = City::where('name', '=', trim($city))->first(['id']);
//                        $cityId = $ecity->id;
//                        $capacity = $row->td[2]->a;
//
//                        \DB::table('sports_teams')->insert([
//                        'sport_id' => $sportId,
//                        'city_id' => $cityId,
//                        'team' => $team,
//                        'homepark' => $homepark,
//                        'capacity' => $capacity,
//                        'created_at' => time()
//                        ]);
//                    }
                
            }
            $i++;

        }

    }

}

