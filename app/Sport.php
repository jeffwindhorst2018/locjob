<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property string $acronym
 * @property string $fullname
 * @property string $source
 * @property string $created_at
 * @property string $updated_at
 * @property SportsTeam[] $sportsTeams
 */
class Sport extends Model
{
    /**
     * @var array
     */
    protected $fillable = ['acronym', 'fullname', 'source', 'datafile', 'created_at', 'updated_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function sportsTeams()
    {
        return $this->hasMany('App\SportsTeam');
    }
}
