<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);
            view()->composer('*', function($view)
            {
                if(\Auth::check() && \Auth::User()->hasRole('Super Admin')) {
                    $todoStatusCounts = \App\Todo::get()->countBy('status_id');
                    $todoStatuses = \App\TodoStatus::all();
                    $todoUsers = \Auth::User()->role('Super Admin')->pluck('name', 'id');
                    $todoCategories = \App\TodoCategory::select('id', 'category')->get();
                    $view->with(compact(['todoStatusCounts', 'todoStatuses', 'todoUsers', 'todoCategories']));
                }
            });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
