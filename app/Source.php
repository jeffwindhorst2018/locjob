<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property string $source
 * @property boolean $required_linkback
 * @property string $linkback
 * @property string $created_at
 * @property string $updated_at
 */
class Source extends Model
{
    /**
     * @var array
     */
    protected $fillable = ['source', 'required_linkback', 'linkback', 'created_at', 'updated_at'];

}
