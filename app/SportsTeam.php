<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property int $sport_id
 * @property int $city_id
 * @property string $league
 * @property string $division
 * @property string $team
 * @property string $homepark
 * @property string $created_at
 * @property string $updated_at
 * @property City $city
 * @property Sport $sport
 */
class SportsTeam extends Model
{
    /**
     * @var array
     */
    protected $fillable = ['sport_id', 'city_id', 'league', 'division', 'team', 'homepark', 'capacity', 'created_at', 'updated_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function city()
    {
        return $this->belongsTo('App\City');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function sport()
    {
        return $this->belongsTo('App\Sport');
    }
}
