<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property int $user_id
 * @property int $status_id
 * @property string $title
 * @property string $description
 * @property string $created_at
 * @property string $updated_at
 * @property TodoStatus $todoStatus
 * @property User $user
 */
class Todo extends Model
{
    /**
     * @var array
     */
    protected $fillable = ['user_id', 'status_id', 'title', 'description', 'created_at', 'updated_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function todoStatus()
    {
        return $this->belongsTo('App\TodoStatus', 'status_id');
    }

    public function todoCategory()
    {
        return $this->belongsTo('App\TodoCategory', 'todo_category_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
