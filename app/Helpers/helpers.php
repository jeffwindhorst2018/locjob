<?php

function _pr($d, bool $end = false) {
    switch (gettype($d)) {
        case ('array'):
            echo '<pre>';
            print_r($d);
            echo '</pre>';
        case ('object'):
        case ('boolean'()):
            echo '<pre>';
            var_dump($d);
            echo '</pre>';
        default:
            echo $d;
    }
    ($end === true) ?: exit ;
}
