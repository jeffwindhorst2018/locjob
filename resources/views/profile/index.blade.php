@extends('layouts.app')

@section('title', '| User Profile')

@section('content')

<div class="row">
    <div id="user-profile-container" class="container">
        <div class="accordion" id="lifestyle-accordion">
            @foreach($lifestyleChoices as $choice)
            <div class="card">
                <div class="card-header" id="sports-card">
                    <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapseSports" aria-expanded="false" aria-controls="collapseSports">
                        {{ $choice->choice_table }}
                    </button>
                </div>
            </div>
            <div id="collapseOne" class="collapse show" aria-labelledby="sports-card" data-parent="#lifestyle-accordion">
              <div class="card-body">
                  <ul>
                      @foreach($sports as $sport)
                      <li>{{ $sport->acronym }}</li>
                      @endforeach
                  </ul>
              </div>
            </div>
            @endforeach
        </div>
    </div>
</div>
@endsection

