@extends('layouts.app')

@section('title', '| Job Listings')

@section('content')
<div class="container">
    <div id="job-listings-meta">
        <ul>
            <li><strong>City:</strong> {{ $city[0]->name }}</li>
            <li><strong>Jobs:</strong> {{ $jobs->count() }}</li>
        </ul>
    </div>
</div>

<div id="job-listings" class="row">

    @foreach($jobs as $job)
    <div class="job-listing container">
        <h3>Job Title: <a href="http://{{ $job->linkback }}" target="_blank">{{ $job->title }}</a></h3>
        <strong>Company:</strong> {{ $job->company }}
        <br>
        Est. Salary{{ $job->salary }}
        <p>
            <strong>Description</strong>
            <br>
            {{ $job->description }}
        </p>
        <strong>Posted:</strong> {{ $job->created_at->diffForHumans() }}
    </div>
    @endforeach
</div>
@endsection
