{{ Form::open(array('route' => 'skill.search')) }}

<div class="form-group clearfix">
    {{ Form::label('skills', 'Skills: ', array('class' => 'float-left')) }}
    {{ Form::text('skills', null, array('class' => 'form-control float-left')) }}
    {{ Form::submit('Search', array('class' => 'btn btn-primary float-left')) }}
</div>

{{ Form::close() }}
