<div id="map-sidebar">
    <h3>City Details<span class="pull-right">&times;</span></h3>
    <ul>
        <li class="detail-name"><strong>Name: </strong><span></span></li>
        <li class="detail-population"><strong>Population: </strong><span></span></li>
        <li class="detail-growth"><strong>Growth: </strong><span></span></li>
        <li class="detail-coords"><strong>Long/Lat: </strong><span></span></li>
        <li class="detail-jobs"><strong>Jobs: </strong><span></span></li>
    </ul>
    <p id="sidebar-lifestyle">
        <a class="btn btn-primary" data-toggle="collapse" href="#sports-data" role="button" aria-expanded="false" aria-controls="collapseExample">
            Sports <span class="pull-right team-count"></span>
        </a>
    </p>
    <div class="collapse" id="sports-data">
        <div class="card card-body">
            Sports NFO
        </div>
    </div>
</div>
