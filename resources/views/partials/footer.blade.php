<div class="row" id="footer">
    <div class="col-md-12">
        <div class="panel w-100">
            Powerd By Laravel v{{ App::VERSION() }} <span id="branch-info" class="ml-5">Branch: {{ $branchname }}</span>
            <span class="pull-right">&copy; {{ date('Y') }}</span>
        </div>
    </div>
</div>