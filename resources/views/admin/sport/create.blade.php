@extends('layouts.app')

@section('title', '| Create New Sport')

@section('content')
<div class="row">
    <div class="col-md-8 col-md-offset-2">
        <h1>Create New Sport</h1>
        <hr>
        @include('form')
        {{ Form::open(array('route' => 'sports.store')) }}
        <div class="form-group">
            {{ Form::label('acronym', 'Acronym') }}
            {{ Form::text('acronym', null, array('class' => 'form-control')) }}
            <br>

            {{ Form::label('fullname', 'Fullname') }}
            {{ Form::textarea('fullname', null, array('class' => 'form-controller')) }}
            <br>
            
            {{ Form::label('source', 'Source') }}
            {{ Form::text('source', null, array('class' => 'form-controller')) }}
            <br>
            
            {{ Form::label('datafile', 'Data File') }}
            {{ Form::text('datafile', null, array('class' => 'form-controller')) }}
            <br>
            
            {{ Form::submit('Create Sport', array('class' => 'btn btn-success btn-lg btn-block')) }}
        </div>
        {{ Form::close() }}

    </div>
</div>
@endsection
