@extends('layouts.app')

@section('title', '| View Sport')

@section('content')

<div class="container">
    
    <h1>{{ $sport->acronym }} - {{ $sport->fullname }}</h1>
    <hr>
    <ul>
        <li><strong>Source:</strong> {{ $sport->source }}</li>
        <li><strong>Datafile:</strong> {{ $sport->datafile }}</li>
    </ul>
    <hr>
    {!! Form::open(['method' => 'DELETE', 'route' => ['sports.destroy', $sport->id] ]) !!}
    <a href="{{ url()->previous() }}" class="btn btn-primary">Back</a>
    @can('Edit Sport')
    <a href="{{ route('sport.edit', $sport->id) }}" class="btn btn-info" role="button">Edit</a>
    @endcan
    @can('Delete Sport')
    {!! Form::submit('Delete', ['class' => 'btn btn-danger']) !!}
    @endcan
    {!! Form::close() !!}
</div>

@endsection
