@extends('layouts.app')
@section('content')
<div id="sport-container" class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading"><h3>Sports</h3></div>
                <div class="panel-heading">
                    Page {{ $sports->currentPage() }} of {{ $sports->lastPage() }}
                    <span class="pull-right"><strong>Total sports: </strong>{{ $totalSports }}</span>
                </div>
                @foreach ($sports as $sport)
                <div class="panel-body">
                    <ul>
                        <li>
                            <a href="{{ route('sports.show', $sport->id) }}"><b>{{ $sport->acronym }}, {{ $sport->fullname }}</b></a>
                        </li>
                    </ul>
                </div>
                @endforeach
            </div>
            <div class="text-center">
                {!! $sports->links() !!}
            </div>
        </div>
    </div>
</div>
@endsection
