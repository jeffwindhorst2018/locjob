@extends('layouts.app')

@section('title', '| View Todo')

@section('content')

<div class="container">

    <h1>&quot;{{ $todo->title }}&quot;</h1>
    <hr>
    <ul>
        <li><strong>Description:</strong> {{ $todo->description }}</li>
        <li><strong>Status:</strong> {{ $todo->TodoStatus->status }}</li>
    </ul>

    <hr>
    {!! Form::open(['method' => 'DELETE', 'route' => ['todos.destroy', $todo->id] ]) !!}
    <a href="{{ url()->previous() }}" class="btn btn-primary">Back</a>
    @role('superadmin')
    <a href="{{ route('todos.edit', $todo->id) }}" class="btn btn-info" role="button">Edit</a>
    {!! Form::submit('Delete', ['class' => 'btn btn-danger']) !!}
    @endrole
    {!! Form::close() !!}
</div>

@endsection

