@extends('layouts.app')
@section('content')

<div class="container">
    <h1>{{ env('APP_NAME') }} Todo List</h1>
    <!-- <a class="btn btn-success" href="javascript:void(0)" id="createNewTodo"> Create Todo</a> -->
    <table class="table table-bordered data-table">
        <thead>
            <tr>
                <th>Title</th>
                <th>Description</th>
                <th>Category</th>
                <th>Status</th>
                <th>Assigned To</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
        </tbody>
    </table>
</div>


<script type="text/javascript">
  $( document ).ready(function() {

      $.ajaxSetup({
          headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          }
    });

    // Setup PHP provider variables in JS
    var statuses = JSON.parse('{!! json_encode($todoStatuses) !!}');
    var table = $('.data-table').DataTable({
        processing: true,
        serverSide: true,
        ajax: "{{ route('ajaxtodos.index') }}",
        columns: [
            {data: 'title', name: 'title'},
            {data: 'description', name: 'description'},
            {data: 'todo_category'},
            {data: 'todo_status', name: 'todo_status'},
            {data: 'user', name: 'user'},
            {data: 'action', name: 'action', orderable: false, searchable: false},
        ]
    });

    $('#createNewTodo').click(function () {
        $('#saveBtn').val("create-todo");
        $('#todo_id').val('');
        $('#todoForm').trigger("reset");
        $('#modelHeading').html("Create Todo");
        $('#ajaxModel').modal('show');
    });

    $('body').on('click', '.editTodo', function () {
      var todo_id = $(this).data('id');
      $.get("{{ route('ajaxtodos.index') }}" +'/' + todo_id +'/edit', function (data) {
          $('#modelHeading').html("Edit Todo");
          $('#saveBtn').val("edit-user");
          $('#ajaxModel').modal('show');
          $('#todo_id').val(data.id);
          $('#title').val(data.title);
          $('#description').val(data.description);
          $('#category').val(data.todo_category_id);
          $('#status').val(data.status_id);
          $('#user').val(data.user_id);
      })
   });

    $('#saveBtn').click(function (e) {
        e.preventDefault();
        $(this).html('Sending..');

        $.ajax({
          data: $('#todoForm').serialize(),
          url: "{{ route('ajaxtodos.store') }}",
          type: "POST",
          dataType: 'json',
          success: function (data) {

              $('#todoForm').trigger("reset");
              $('#ajaxModel').modal('hide');
              table.draw();

          },
          error: function (data) {
              console.log('Error:', data);
              $('#saveBtn').html('Save Changes');
          }
      });
    });

    $('body').on('click', '.deleteTodo', function () {

        var todo_id = $(this).data("id");
        confirm("Are You sure want to delete !");

        $.ajax({
            type: "DELETE",
            url: "{{ route('ajaxtodos.store') }}"+'/'+todo_id,
            success: function (data) {
                table.draw();
            },
            error: function (data) {
                console.log('Error:', data);
            }
        });
    });

  });
</script>

@endsection
