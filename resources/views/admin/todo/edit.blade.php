@extends('layouts.app')

@section('title', '| Edit Todo')

@section('content')

<div class="container">


    <h1>Edit: &#39;{{ $todo->title }}&#39;</h1>
    <hr>
    {{ Form::open(['route' => 'todos.update', 'class' => 'form-group', 'id' => 'add-todo-form']) }}
    <div class="row">
        <div>
            {{ Form::hidden('id', $todo->id) }}
            
            {{ Form::label('title', 'Title') }}
            {{ Form::text('title', $todo->title, ['placeholder'=> 'Title', 'class' => 'form-control']) }}
            <br>

            {{ Form::label('description', 'Description') }}
            {{ Form::textarea('description', $todo->description, [
                    'placeholder' => 'Description',
                    'class' => 'form-control',
                    'rows' => '2', 'cols' => '30'
            ]) }}
            <br>

            {{ Form::label('status', 'Status') }}
            {{ Form::select('status', $statuses, $todo->status_id, ['placeholder' => 'Status']) }}
            <br>

            {{ Form::label('user', 'User') }}
            {{ Form::select('user', $todoUsers, $todo->user_id, ['placeholder' => 'Assign to']) }}
            <br>

            {{ Form::label('category', 'Category') }}
            {{ Form::select('category', $categories, $todo->category_id, ['placeholder' => 'Category']) }}
            <br>

            {{ Form::submit('Update Todo', ['class' => 'btn btn-success']) }}
        </div>
    </div>
    {{ Form::close() }}
</div>

@endsection

