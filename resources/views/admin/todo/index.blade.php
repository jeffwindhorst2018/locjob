@extends('layouts.app')
@section('content')
<div id="todo-container" class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="row justify-content-md-center">
                    <div id="message" class="position-fixed ml-10 border border-primary">
                        Messge
                    </div>
                </div>
                <div class="panel-heading">
                    <h3>Todo List</h3>
                </div>
                <div class="panel-heading">
                    Page {{ $todos->currentPage() }} of {{ $todos->lastPage() }}
                    <span class="pull-right"><strong>Total todos: </strong>{{ $totalTodos }}</span>
                </div>


                <div class="panel-body">
                    <ul class="list-group list-group-hover list-group-striped" id="todo-list">
                        @foreach ($todos as $todo)
                        {{-- <Pre>
                            {{ print_r($todo->toArray()) }}
                        </pre> --}}
                        <li class="pull-left">
                            <a href="{{ route('todos.show', $todo->id) }}"><strong>{{ $todo->title }}</strong></a>
                            <a href="/admin/todo/destroy/{{ $todo->id }}"
                               class="pull-right ml-5 mr-2 p-1 delete-todo"
                               data-todo-id = {{ $todo->id }}
                               data-toggle ="confirmation"
                               data-title ="Are you sure you want to remove todo &#39;{{ $todo->title}}&#39;"
                               data-placement = "left"
                               >
                                @svg('iconmonstr-x-mark-4', ['class' => 'svg-sm svg-red'])
                            </a>
                            
                            {{ Form::select('category', $todoCategories->pluck('category', 'id')),
                                                $todo->todoCategory->id ?? 0,
                                                [
                                                    'placeholder' => 'Category',
                                                    'class' => 'pull-right todo-category mt-2 mr-2',
                                                    'data-todo-id' => $todo->id,
                                                    'data-field' => 'todo_category_id'
                                                ]
                                            }}
                            {{ Form::select('status', $todoStatuses->pluck('status', 'id')),
                                                $todo->todoStatus->id ?? 0,
                                                [
                                                    'placeholder' => 'Status',
                                                    'class' => 'pull-right todo-status mt-2 mr-2',
                                                    'data-todo-id' => $todo->id,
                                                    'data-field' => 'status_id'
                                                ]
                                            }}
                            {{ Form::select('user', $todoUsers,
                                                $todo->User->id ?? 0,
                                                [
                                                    'placeholder' => 'Assign',
                                                    'class' => 'pull-right todo-assign mt-2 mr-2',
                                                    'data-todo-id' => $todo->id,
                                                    'data-field' => 'user_id'
                                                ]
                                            ) }}
                            
                        </li>
                        @endforeach
                    </ul>
                </div>
            </div>
            <div class="text-center">
                {!! $todos->links() !!}
            </div>
        </div>
    </div>
</div>
<script>
$(document).ready(function(){
    $('.list-group select').on('change', function(e){
        var loader = $(this).siblings('span.loader');
        loader.removeClass('d-none')
        $.post( "{{ route('todos.update-ajax') }}", { field: $(this).data('field'), todoId: $(this).data('todo-id'), 'val': $(this).val() })
            .done(function( data ) {
                loader.addClass('d-none');
        });
    });

    // Delete TODO
    $('.delete-todo').on('click', function(e){ e.preventDefault(); });
    $('[data-toggle=confirmation]').confirmation({
        rootSelector:'[data-toggle=confirmation]',
        singleton: true,
        copyAttributes: '',
        onConfirm: function(e) {
            tid=$(this).data('todo-id');
            $.post("{{ route('todo.destroy') }}", {'id':tid}, function(data){
                jdata= JSON.parse(data);
                console.log('DATA ' + jdata.message);
            });
        }
    });

});
</script>
@endsection

