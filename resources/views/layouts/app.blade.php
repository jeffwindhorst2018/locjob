<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    
    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Styles -->
    <link href="{{ asset('css/jquery-ui.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/ajax-loaders.css') }}" rel="stylesheet">
    
    <!-- Scripts -->
    <!--
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/jquery.validate.js"></script>
    <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js" defer></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>
    <script src="{{ asset('js/app.js') }}" defer></script>
    -->

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.min.css" />
    <link href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css" rel="stylesheet">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/jquery.validate.js"></script>
    <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js" defer></script>
    <!--
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" defer></script>
    -->
    <script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js" defer></script>
    <script src="{{ asset('js/bootstrap-confirmation.min.js') }}" defer></script>
    
    <!-- Fonts -->
    <link rel="dns-prefetch" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.bundle.min.js" integrity="sha384-xrRywqdh3PHs8keKZN+8zzc5TX0GRTLCcmivcbNJWm2rs5C8PRhcEn3czEjhAO9o" crossorigin="anonymous"></script>

    <script>
        $(document).ready(function(){
            window.Laravel = {!! json_encode([
                'csrfToken' => csrf_token(),
            ]) !!};
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            var csrf = $('meta[name="csrf-token"]').attr('content');
        });
    </script>
    <script src="https://use.fontawesome.com/9712be8772.js"></script>
</head>
<body>

    @php
    // Branchname to display in the footer.
    $stringfromfile = file('../.git/HEAD', FILE_USE_INCLUDE_PATH);
    $firstLine = $stringfromfile[0]; //get the string from the array
    $explodedstring = explode("/", $firstLine, 3); //seperate out by the "/" in the string
    $branchname = $explodedstring[2]; //get the one that is always the branch name
    @endphp

    <div id="app">

        <nav class="navbar navbar-expand-md navbar-light navbar-laravel">
            <div class="container">
                <a class="navbar-brand" href="{{ url('/') }}">
                    {{ config('app.name', 'Laravel') }}
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <!-- Left Side Of Navbar -->
                    @hasanyrole('Super Admin|Admin')
                        @if (!Auth::guest())
                            @include('layouts.partials.admin_nav')
                        @endif
                    @endhasanyrole

                    <!-- Right Side Of Navbar -->
                    <ul class="navbar-nav ml-auto pull-right">
                        <!-- Authentication Links -->
                        @guest
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                            </li>
                        @else
                            <li class="nav-item dropdown">
                                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" onclick="event.preventDefault();
                                    $('#logout-link').show();"
                                    role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    {{ Auth::user()->name }} <span class="caret"></span>
                                </a>

                                <div id="logout-link" class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item" href="{{ route('profile') }}">Profile</a>
                                    <a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                </div>
                            </li>
                       @endguest
                    </ul>
                </div>
            </div>
        </nav>

        @if(Session::has('flash_message'))
        <div class="container-fluid">
            <div class="alert alert-success">
                <em> {!! session('flash_message') !!}</em>
            </div>
        </div>
        @endif
        
        <div class="row">
            <div class="w-100">
                @include ('errors.list')
            </div>
        </div>
        
        <main class="py-4 container-fluid">
            @if( Request::path() == 'skill/search')
            @include('partials.map_sidebar')
            @endif
            @yield('content')
        </main>
        
        
    </div>
    <div class="container-fluid" id="footer-wrapper">
        <footer class="purple-gradient">
            @include ('partials.footer')
        </footer>
    </div>
    
    <script src="{{ asset('js/app.js') }}"></script>
    <script>
        $(document).ready(function(){
            // ADD TODO SCRIPTS
            ///////////////////////////////////////////////

            // Open Create Todo Modal
            $('#createNewTodo').click(function () {
                $('#saveBtn').val("create-todo");
                $('#todo_id').val('');
                $('#todoForm').trigger("reset");
                $('#modelHeading').html("Create Todo");
                $('#ajaxModel').modal('show');
            });

            // Save New Todo Data
            $('#todoSaveBtn').click(function (e) {
                e.preventDefault();
                $(this).html('Sending..');

                $.ajax({
                  data: $('#todoForm').serialize(),
                  url: "{{ route('ajaxtodos.store') }}",
                  type: "POST",
                  dataType: 'json',
                  success: function (data) {

                      $('#todoForm').trigger("reset");
                      $('#ajaxModel').modal('hide');
                      if('/admin/todo' == window.location.pathname) {
                          table.draw();
                      }

                  },
                  error: function (data) {
                      console.log('Error:', data);
                      $('#saveBtn').html('Save Changes');
                  }
              });
            });
        });
    </script>
</body>
</html>
