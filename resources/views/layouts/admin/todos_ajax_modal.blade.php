<div class="modal fade" id="ajaxModel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="modelHeading">Todo Ajax Add</h4>
            </div>
            <div class="modal-body">
                <form id="todoForm" name="todoForm" class="form-horizontal">
                    <input type="hidden" name="todo_id" id="todo_id">
                    <div class="form-group">
                        <label for="name" class="col-sm-3 control-label">Title</label>
                        <div class="col-sm-12">
                            <input type="text" class="form-control" id="title" name="title" placeholder="Enter Name" value="" maxlength="50" required="">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-3 control-label">Description</label>
                        <div class="col-sm-12">
                            <textarea id="description" name="description" required="" placeholder="Enter Description" class="form-control"></textarea>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="status" class="col-sm-3 control-label">Status:</label>
                        <div class="col-sm-12">
                            {{ Form::select('status', $todoStatuses->pluck('status', 'id')->map('ucwords', 'status'), null, [
                                                                                                                                'id' => 'status',
                                                                                                                                'placeholder' => 'Status',
                                                                                                                                'class' => 'form-control'
                                                                                                                            ]) }}
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="category" class="col-sm-3 control-label">Category:</label>
                        <div class="col-sm-12">
                            {{ Form::select('category', $todoCategories->pluck('category', 'id')->map('ucwords', 'category'), null, [
                                                                                                                                        'id' => 'category',
                                                                                                                                        'placeholder' => 'Category',
                                                                                                                                        'class' => 'form-control'
                                                                                                                                    ]) }}
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="user" class="col-sm-3 control-label">Assigned to:</label>
                        <div class="col-sm-12">
                            {{ Form::select('user', $todoUsers->map('ucwords', 'name'), null, [
                                                                                                'id' => 'user',
                                                                                                'placeholder' => 'Assign to',
                                                                                                'class' => 'form-control'
                                                                                            ]) }}
                        </div>
                    </div>

                    <div class="col-sm-offset-2 col-sm-10">
                        <button type="submit" class="btn btn-primary" id="todoSaveBtn" value="create">Save changes</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>