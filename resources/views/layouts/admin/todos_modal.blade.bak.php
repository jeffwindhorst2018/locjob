<button type="button" class="btn btn-success btn-sm mr-2" data-toggle="modal" data-target="#todo-modal">
    &#x002B;
</button>

<div class="modal container-fluid mt-0" id="todo-modal" tabindex="-1" role="dialog" aria-labelledby="add-todo-btn" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="add-todo-btn">Add TODO</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">

                <div id="error-wrapper" class="alert alert-danger" hidden>
                    <ul id="form-errors" class="mb-0"></ul>
                </div>

                <div class="container-fluid" id="todo-modal-body">
                    {{ Form::open(['route' => 'todos.store', 'class' => '', 'id' => 'add-todo-form']) }}
                        <div class="row mb-2">
                            <label for="title" class="mr-2">Title: </label>
                            {{ Form::text('title', null, ['placeholder'=> 'Title', 'class' => 'form-control']) }}
                        </div>
                        <div class="row mb-2">
                            <label for="description" class="mr-2">Description: </label>
                            {{ Form::textarea('description', null, [
                                                                'placeholder' => 'Description',
                                                                'class' => 'form-control',
                                                                'rows' => '2', 'cols' => '30']) }}
                        </div>
                        <div class="row mb-2">
                            <label for="status" class="mr-2">Status:</label>
                            {{ Form::select('status', $todoStatuses->pluck('status', 'id')->map('ucwords', 'status'), null, ['placeholder' => 'Status']) }}
                        </div>
                        <div class="row">
                            <label for="Assign to" class="mr-2">Assign to:</label>
                            {{ Form::select('user', $todoUsers->map('ucwords'), null, ['placeholder' => 'Assign to']) }}
                        </div>

                        <div class="row border-bottom">
                            <label for="category:" class="mr-2">Category: </label>
                            {{ Form::select('category', $todoCategories->pluck('category', 'id'), null, ['placeholder' => 'Category']) }}
                        </div>

                        <div class="row mt-2">
                            <button type="button" id="create-btn" class="btn btn-success mr-3">Create</button>
                            <button type="button" id="create-add-btn" class="btn btn-success mr-3">Create and Add</button>
                        </div>
                    {{ Form::close() }}
                </div>

            </div>
        </div>
    </div>
</div>

<script>
// Fix multiple modal backdrops
$(document).on('show.bs.modal', function(e) {
    $(".modal").on("shown.bs.modal", function () {
        if ($(".modal-backdrop").length > 1) {
            $(".modal-backdrop").not(':first').remove();
        }
    });
});
$(document).ready(function(){
    $('#create-btn, #create-add-btn').on('click', function(e) {
        $('#add-todo-form').append('<input type="hidden" name="mode" value="'+$(this).attr('id')+'" />');
        formSerialize=$('#add-todo-form').serialize();
        $.post('{{ route("todos.store") }}', formSerialize, function(data) {
            if(data.error) {
                $.each(data.error, function(key, value){

                    $('#form-errors').append('<li>'+value +'</li>');
                });
                $('#error-wrapper').removeAttr('hidden');
            } else {
                console.log('CHECK ' + data.title);
                showUrl = "{{ url('/admin/todo/show/') }}";
                if(data.mode == 'create-btn') {
                    $('#todo-list').append("<li class='pull-left'><a href='" + showUrl + "/" + data.id + "'><strong>"+ data.title+"</strong></a>")
                    $('#todo-modal').modal('hide');
                    $(".modal-backdrop").remove();
                } else if($(this).attr('id') == 'create-add-btn') {
                    console.log("Inside Create Add");
                }
            }
        });
    });
});
</script>

