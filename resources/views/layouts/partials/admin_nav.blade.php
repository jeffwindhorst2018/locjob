
<ul id="admin-nav" class="navbar-nav mr-2">
    <li class="nav-item dropdown">
        <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" onclick="event.preventDefault();
            $('#admin-links').show();"
           role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
            Admin <span class="caret"></span>
        </a>
        <div id="admin-links" class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
            <a class="dropdown-item" href="{{ route('city.index') }}">Cities</a>
            <a class="dropdown-item" href="{{ route('permissions.index') }}">Permissions</a>
            <a class="dropdown-item" href="{{ route('roles.index') }}">Roles</a>
            <a class="dropdown-item" href="{{ route('users.index') }}">Users</a>
            <a class="dropdown-item" href="{{ route('todos.index') }}">TODO List</a>
        </div>
    </li>
</ul>

{{-- @include('layouts.admin.todos_modal') --}}
@include('layouts.admin.todos_ajax_modal')
<a href="javascript:void(0)" id="createNewTodo" class="btn btn-success">&#x002B;</a>
<ul class="mt-0 mb-2 ml-3" id="todo-links">
    <li class="pull-left mr-2">
        <a href="'#" type="button" id="todos-total-pill" class="badge badge-primary" data-toggle="tooltip" data-placement="top" title="Total Todos">
            <span class="badge badge-light">{{ $totalTodos  }}</span>
        </a>
    </li>
    @foreach($todoStatuses as $key => $status)
    <li class="pull-left mr-2">
        <a href="'#" type="button" id="todos-{{ $status['status'] }}-pill" class="badge badge-primary" data-toggle="tooltip" data-placement="top" title="{{ ucwords($status['status']) }}">
          <span class="badge badge-light">
              {{ isset($todoStatusCounts[$status->id]) ? $todoStatusCounts[$status->id] : 0 }}
          </span>
        </a>
    </li>
    @endforeach
</ul>

<script>
//$(document).ready(function(e){
//    $('[data-toggle="tooltip"]').tooltip({'delay': { show: 0, hide: 0 } });
//});
</script>