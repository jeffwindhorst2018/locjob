@extends('layouts.app')

@section('title', '| Search Results')

@section('content')

<div class='col'>

    <h1 id="results-header"><i class='fa fa-key'></i> Search Results</h1>

    <ul class="skill-list">
        <li>Terms: {{ $skills }}</li>
        <li>Found {{ number_format($jobTotal, 0) }} job&#39;s in {{ number_format($cityTotal, 0) }} cities.</li>
    </ul>

    @include('partials.map_sidebar')
    
    <div id="map-wrapper">
        <div id="map" style="width: 100%; height: 500px; display: block; border: 1px solid red;"></div>
    </div>

<script language="javascript" src="https://maps.googleapis.com/maps/api/js?sensor=true&v=3"></script>

<script>
        // todo move this to it's own file
        var cities = <?php echo json_encode($jcities); ?>;

        function initMap() {
            var map;
            var bounds = new google.maps.LatLngBounds();
            var mapOptions = {
                mapTypeId: 'roadmap',
                zoom: 57
            };

            // Display a map on the web page
            map = new google.maps.Map(document.getElementById("map"), mapOptions);
            map.setTilt(50);

            var infoWindow = new google.maps.InfoWindow(), markers, i;
            var markers = cities;
            
            // Place each marker on the map
            for( i = 0; i < markers.length; i++ ) {

                var position = new google.maps.LatLng(markers[i].latitude, markers[i].longitude);
                bounds.extend(position);
                marker = new google.maps.Marker({
                    position: position,
                    map: map,
                    title: markers[i].name + ' ' + markers[i].state
                });
                
                // Add info window to marker
                google.maps.event.addListener(marker, 'click', (function(marker, i) {

                    return function() {
                        
                        
                        if(markers[i].growth > 0){
                                    arrow = "&#8593;";
                                    arrowClass = "arrow-class-up";
                        } else {
                                    arrow = "&#8595;";
                                    arrowClass = "arrow-class-down";
                        }
                        perGrowth = '%' + markers[i].growth.replace('-', '');


                        // jobCount = markers[i].job_total.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                        var infoWindowContent =
                            '<div id="infoWindow-'+ markers[i].cityId +'" class="info-content">' +
                            '<h3>' + markers[i].name + ', ' + markers[i].state + '</h3>' +
                            '<ul>' +
                            '<li><strong>Population: </strong>' + markers[i].population + '</li>' +
                            '<li><strong>Job count: </strong>' + markers[i].job_count + '</li>' +
                            '<li><strong>Growth: </strong>' + perGrowth + ' <span class="'+ arrowClass +'">' + arrow + '</span></li>' +
                            '<li><a href="javascript:void(0);" id="city-'+ markers[i].cityId +'" class="pull-right" data-city-id="'+ markers[i].cityId+'">Details</a></li>' +
                            '</ul>' +
                            '</div>';

                        var jobCount = markers[i].job_count;
                        infoWindow.setContent(infoWindowContent);
                        infoWindow.open(map, marker);
                        var cityId = markers[i].cityId;

                        //
                        // This says that the infoWindow that was clicked is loaded.
                        //
                        google.maps.event.addListener(infoWindow, 'domready', function(){
                            $('#infoWindow-' + cityId).on('click', 'a', function(e){
                                
                                e.preventDefault();
                                
                                 $('#map-sidebar').show();
                                 cityId=$(this).data('city-id');
                                 // $.post('/city/details', { id: cityId, '_token': csrf }, function(data){
                                 $.post('/city/details', { id: cityId }, function(data){
                                    jdata = $.parseJSON(data);
                                    console.log('JDATA NAME ' + jdata.name);
                                    console.dir(jdata);
                                    // $('.detail-name span').text(jdata.name + ', ' + jdata.state.name);
                                    $('.detail-name span').text(jdata.name);
                                    $('.detail-population span').text(jdata.population);
                                    $('.detail-growth span').text('%' + jdata.growth_from_2000_to_2013);
                                    $('.detail-coords span').text(jdata.longitude + '/' + jdata.latitude);
                                    $('.detail-jobs span').html('<a href="/jobs/'+ cityId+'" class="cityjobs">' + jobCount + '</a>');
                                    // // Sports NFO
                                    sportsData = '<ul>';
                                    $('.team-count').text(jdata.sports_team.length);
                                    for(i=0; i<jdata.sports_team.length; i++) {
                                        sportsData += '<li><strong>' + jdata.sports_team[i].team + '<span class="pull-right">' + jdata.sports_team[i].homepark + '</span></strong></li>';
                                    }
                                    sportsData += '</ul>';
                                    $('#sports-data div').html(sportsData);
                                 });
                            });


                            //
                            // TODO Fix this binding
                            //
                            $('#map-sidebar h3').on('click',  'span', function() {
                                $('#map-sidebar').hide();
                           });
                            
                        });

                    };
                })(marker, i));

                // Center the map to fit all markers on the screen
                map.fitBounds(bounds);
            }

        }
        // Load initialize function
        google.maps.event.addDomListener(window, 'load', initMap);

        $(document).ready(function(){
            $('.info-content ul li a').on('click', function(){
                console.log('clicked');
            });
        });
    </script>

</div>

@endsection

