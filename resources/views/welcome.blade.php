
@extends('layouts.app')

@section('title', ' | Home Page')

@section('content')
<div class="flex-center position-ref full-height">
    <div class="content">
        
        <div id="search-form-wrapper">
            @include('partials.search-form')
        </div>
        
    </div>
</div>
@endsection
