<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/profile', 'ProfileController@index')->name('profile');

/**
 * Admin routes
 */
Route::get('/admin', 'AdminController@index')->name('admin');
Route::get('/admin/city', 'CityController@index')->name('city.index');
Route::get('/admin/city/create', ['middleware' => 'auth'], 'CityController@create')->name('city.create');

// EXAMPLE: Working middleware example
Route::get('/admin/todo', ['middleware' => 'auth', 'uses' => 'TodoController@index'])->name('todos.index');
Route::get('/admin/todo/show/{todoId}', 'TodoController@show')->name('todos.show');
Route::post('/admin/todo/store', 'TodoController@store')->name('todos.store');
Route::get('/admin/todo/store', 'TodoController@store')->name('todos.store');



Route::post('/admin/todo/destroy', ['middleware' => 'auth', 'uses' => 'TodoController@destroy'])->name('todo.destroy');
Route::get('/admin/todo/edit/{todo}', ['middleware' => 'auth', 'uses' => 'TodoController@edit'])->name('todos.edit');
Route::post('/admin/todo/update/', ['middleware' => 'auth', 'uses' => 'TodoController@update'])->name('todos.update');
Route::post('/admin/todo/update-ajax/', ['middleware' => 'auth', 'uses' => 'TodoController@updateAjax'])->name('todos.update-ajax');

// Ajax todo routes
Route::resource('/admin/ajaxtodos','TodoAjaxController');

Route::get('jobs/{cityId}', 'JobController@show')->name('jobs.city');
Route::post('skill/search', 'JobController@search')->name('skill.search');
Route::post('city/details', 'CityController@show')->name('city.details');
Route::get('permissions', ['middleware' => 'auth'], 'PermissionController@index')->name('permission.index');
// Route::get('users', ['middleware' => 'auth'], 'UserController@index')->name('users.index');
// Route::delete('/admin/user/destroy/{user}', ['middleware' => 'auth', 'uses' => 'UserController@destroy'])->name('users.destroy');

Route::resource('users', 'UserController');
Route::resource('roles', 'RoleController');
Route::resource('permissions', 'PermissionController');
Route::resource('cities', 'CityController');
