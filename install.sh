#!/bin/bash

# Clone the repository
git clone https://jeffwindhorst2018@bitbucket.org/jeffwindhorst2018/locjob.git

# Update vendors
composer update

# Update credentials in .env
cp .env.example .env

read -n 1 -p "Have you updated the values in your .env file?(y)" fileupdated

if [ "$fileupdated" = "1" ]; then

# At this point the site should be up and running.

# Not sure why this command causes an initial migration failure
    rm app/Console/Commands/ScrapeSports.php

    php artisan migrate

    # Now bring that removed file back from the repo
    git checkout app/Console/Commands/ScrapeSports.php

    php artisan add:city

    # This takes up to 10 minutes
    php artisan db:seed

    # The scrape:sports command could be cleaned up, but I have not gotten back to it.
    php artisan scrape:sports mlb
    php artisan scrape:sports mls
    php artisan scrape:sports nba 
    php artisan scrape:sports nfl 
    php artisan scrape:sports nhl 

    ## If you ever hit this error: Failed to clear cache. Make sure you have the appropriate permissions.
    ##
    ## Run mkdir storage/framework/cache/data
    ##
fi

